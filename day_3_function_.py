#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# câu 1 1.Given the following list l1 = [3, 4, ‘hello’, (4.6, 5.6), [3,7,2,2, [0.3, 5],‘text’], 6, 3, 3.5]. 
# a.Write a recursive function named sum_deep() that get l1 as an argument and recursively calculate the sum of numbers in this list. For example: sum_deep(l1) ⇒ #output: 49.0
# b. Test your sum_deep function with any list
l1=[3,4,'hello',(4.6,5.6),[3,7,2,2,[0.3,5],'text'],6,3,3.5]
def tubb(x):
    t=0;
    for i in x:
        tp=str(type(i))
        if 'float'in tp or 'int'in tp:
            t+=i
    return t
def lis(x):
    t=0
    for i in x:
        tp = str(type(i))
        if 'float'in tp or 'int'in tp:
            t+=i
        if 'tuple' in tp:
            t +=tubb(i)
        if 'list' in tp:
            t += lis(i)
    return t
print('a. ket qua: ',lis(l1))
print('b. nhap list bat ky:')
l2=input()
l2=list(l2)
print('ket qua:',lis(l2))


# In[ ]:


# câu 2
# Write a function that return a list of  max value of two lists as follows:
# l1 = [2,4,6,4,9,4,7]
# l2 = [5,3,5,6,9,7,5]
# max_of_pair(l1,l2) => [5,4,6,6,9,7,7]
# NOTE: check if l1 or l2 exists non-number
l1=[2,4,6,4,9,4,7]
l2=[5,3,5,6,9,7,5]
def max_of_pair(l1,l2):
    print('Without key argument, the maximum list:', max(l1, l2))
    return max_of_pair
print(max(l1,l2))


# In[ ]:


# 3.Given a word_count_data.txt (shared folder), write a function to count word of this file using map and reduce functions.
# GUIDE:  use f = open("word_count_data.txt", "r") and line=f.readline() to open a file and read line-by-line of text file. You may also use 
# lines = f.readlines() to read all lines of file into a list.
f = open(r"D:\university's subject\python learning\week3_1.txt","r")
def sumword(x):
    str=x.readlines()
    print(str)
    lismap=list(map(len,str))
    dem=0
    for i in lismap:
        dem+=i
    return dem-len(str)+1
print(sumword(f))

